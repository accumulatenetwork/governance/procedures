# Server Maintenance

## DNS self update (AWS)

This is a procedure for setting up an AWS EC2 instance to self-update a Route 53 DNS record on boot. Based on [this guide](https://dev.to/aws/amazon-route-53-how-to-automatically-update-ip-addresses-without-using-elastic-ips-h7o).

1. Choose or create a zone in Route 53 that will be used for this. For example, Defi Devs uses accumulate.defidevs.io.
2. Add the following tags to the EC2 instance:
   - Set AUTO_DNS_ZONE to the ID of the Route 53 zone.
   - Set AUTO_DNS_NAME to the **fully qualified** domain name to use for the instance, such as bootstrap.accumulate.defidevs.io.
3. Install the AWS CLI on the instance if it's not already present:
   - `curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"`
   - `apt install unzip && unzip awscliv2.zip && ./aws/install && rm -rf aws awscliv2.zip`
4. Set up the IAM role
   - Create a new IAM role that has permission to update DNS in the zone, for example [iam.json](https://gitlab.com/-/snippets/2508485) (replace the zone ID with the real one).
   - [Attach that role to the instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/attach-iam-role.html).
4. Install the DNS update boot script:
   - Copy [route-53.sh](https://gitlab.com/-/snippets/2508485) to `/var/lib/cloud/scripts/per-boot`. If the instance is running an image other than Amazon Linux, you may have to use a different path.
   - `chmod +x /var/lib/cloud/scripts/per-boot/route-53.sh` to make it executable.
   - Run the script to initialize the DNS entry, or reboot the instance so that it will run.

## Increase root disk size (AWS)

Reference: https://stackoverflow.com/a/69741114

1. Stop the instance.
2. Take a snapshot.
3. Open the volume. Volumes are listed on the Storage tab of the instance.
4. Modify the volume and set the size to the new value.
5. Wait for the volume to reach the `optimizing` state. It's fine if it gets all the way to `completed`.
6. Reboot the VM. **The IP address will have changed.**
7. **You may have to expand the filesystem.** Some images have scripts to do this on boot, but for others such as vanilla Ubuntu you must do it manually. See the reference link.