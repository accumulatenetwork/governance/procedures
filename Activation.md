# Activation

Activation will be executed by the activation team: Ethan, Dennis, and Ben.

### Goals

* Any individual governance committee may initiate a grant disbursment. Such a
  transaction must be approved by 2/3 of the foundation.
* The foundation can add or remove committee members.
* Committees may add or remove their own members.
* The total set of committee members collectively vote on modifying the
  governance ADI.
* Once the initial ACME blocks are issued, all future token issuance is handled
  by staking.

## Initial bootup

1. Stop Factom.
2. Export Factom database into object files.
3. Convert Factoid addresses into LTAs.
4. Convert Factom data entries into WriteData transactions.
5. Build LDAs and chain history from WriteData transactions.
6. Build reserved ADI snapshot from CSVs and Jay Singh's whitelist.
7. Create the genesis payload.
8. Launch the seeds.

### Routing

Below is a balanced routing table. We may want to tweak the routing table to
more evenly distribute the imported Factom data.

```jsonc
{
  "overrides": [{ "account": "staking.acme", "partition": "Directory" }],
  "routes": [
    // Route 6/16 to BVN0 and each 5/16 to BVN1 and BVN 2
    { "partition": "BVN0", "length": 2, "value": 0  }, // 00
    { "partition": "BVN1", "length": 2, "value": 1  }, // 01
    { "partition": "BVN2", "length": 2, "value": 2  }, // 10
    { "partition": "BVN0", "length": 3, "value": 6  }, // 110
    { "partition": "BVN1", "length": 4, "value": 14 }, // 1110
    { "partition": "BVN2", "length": 4, "value": 15 }  // 1111
  ]
}
```

## Post-bootup

* Setup ADIs for foundation board members, committee members, and operators.
  * ADIs are preallocated, owned by `accumulate.acme/book`.
  * The activation team will create a key book, add the activation key, and
    transfer custody of the ADI from the foundation to the new book.

### DN

* ADI `dn.acme`
  * Governed by `dn.acme/operators`
  * Book `book`
    * Page `1`
      * Delegate `dn.acme/operators`
    * Page `2`
      * Delegate `governance.acme/book`
  * Data account `network`
    * Governed by `dn.acme/operators`
  * Data account `globals`
    * Governed by `dn.acme/book`
  * Data account `oracle`
    * Governed by `dn.acme/book`
  * Data account `routing`
    * Governed by `dn.acme/book`

### Foundation

The foundation's ADI, `accumulate.acme`, has control over the governance committees.

* ADI `accumulate.acme`
  * Governed by `accumulate.acme/book`
  * Book `book`
    * Page `1`
      * Delegate `dn.acme/operators`, so the operators can replace foundation board members
    * Page `2`
      * Delegate `<personal>.acme/book` for each foundation board member
      * Threshold 2/3
  * Token account `grant-block`
    * Governed by `accumulate.acme/book` and `governance.acme/book`
    * Issued 60 M ACME
  * Token account `dev-block`
    * Governed by `accumulate.acme/book` and `governance.acme/book`
    * Issued 90 M ACME

### Governance

* ADI `governance.acme`
  * Governed by `governance.acme/book`
  * Book `book`
    * Page `1`
      * Delegate `accumulate.acme/book`, so the foundation board can replace committee members
    * Page `2`
      * Delegate `<personal>.acme/book` for each member of each committee
      * Threshold 2/3
  * Sub-ADI `<committee>` for each committee
    * Governed by `governance.acme/<committee>/book`
    * Book `book`
      * Page `1`
        * Delegate `accumulate.acme/book`, so the foundation board can replace committee members
      * Page `2`
        * Delegate `<personal>.acme/book` for each member of the committee
        * Threshold 2/3

### Staking

* ADI `staking.acme`
  * Governed by `staking.acme/book`
  * Book `book`
    * Page `1`
      * Delegate `dn.acme/operators`
    * Page `2`
      * **Keys? Delegates? Who are the initial key holders?**

### Marketplace

* ADI `marketplace.acme`
  * Governed by `marketplace.acme/book`
  * Book `marketplace`
    * Page `1`
      * **Keys? Delegates? Who are the initial key holders?**

### ACME issuer

* Issuer `ACME`
  * Governed by `staking.acme/book`

# Activation

1. Validators are brought on board and activated.
2. Custody of personal ADIs is transferred to key holders by adding their key
   and instructing them to remove the activation key.
3. `dn.acme/operators` is handed over to the operators (seed keys are removed).
4. The governance committee verifies that the network is operational.
5. The governance committee initiates the dev block disbursal.
6. Activation is announced.
