# Releasing a new version of the protocol

1. Finalize the changelog section for X.Y.
1. If there is not already a `release-X.Y` branch, make one.
1. Create and push a `vX.Y.0` tag.
1. The vX-Y-0 docker image should be built automatically.
1. Create a [release in GitLab][gl-release], including the X.Y changelog. See
   the template below.
1. Update the version in the [network map][net-map].
1. Instruct the operators to update.
1. Activate the new protocol version.

[gl-release]: https://gitlab.com/accumulatenetwork/accumulate/-/releases
[net-map]: https://gitlab.com/accumulatenetwork/network-map/-/blob/main/networkmap.yaml

### Release template

- Tag name: `vX.Y.Z`.
- Title: `Accumulate X.Y.Z`.
- Release notes: Copy from the changelog and reformat as appropriate.
